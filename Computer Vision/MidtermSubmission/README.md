# 2D Feature Tracking
### Jonny Erickson

## MP.1 - Data Buffer Optimization
I first initialized some desired data buffer size, which in the case of my code is two. The data frame receives a new image and checks to see what the size of the data buffer is (how many images are in the data buffer). If the number of images exceeds two, then the first image received (which in our case is now two images ago) is popped off of the data buffer. 
## MP.2 - Keypoint Detection
In this segment the Shi-Tomasi, Harris, FAST, BRISK, ORB, AKAZE, and SIFT detectors are implemented and made selectable by a string. For example:
~~~
string detectorType = "HARRIS"; 
~~~
The program will run using the detector that is chosen here. 

## MP.3 - Keypoint Removal
Here, we are going to focus on a region of interest for keypoints (somewhat similar to what was done in the LiDAR section of the course). However, we only want to focus on keypoints that are on the vehicle directly in front of the ego vehicle. This was implemented simply by putting a rectangle over the vehicle of interest, and then checking if the keypoints are contained by this rectangle. If they are, they are kept. Else, they are removed.
~~~
bool bFocusOnVehicle = true;
cv::Rect vehicleRect(535, 180, 180, 150);
if (bFocusOnVehicle)
{
    vector<cv::KeyPoint> interestRegion;
    for (auto KP : keypoints) 
    {
        if (vehicleRect.contains(KP.pt))
        {
            interestRegion.push_back(KP);
        }
    }
    keypoints = interestRegion;
}
~~~
## MP.4 - Keypoint Descriptors
Now that we have detected keypoints, we need to then find the descriptors that will be used for matching keypoints. We will implement the BRIEF, ORB, FREAK, AKAZE, and SIFT descriptors using a string declaration like what we did for the detector section. 
~~~
string descriptorType = "BRIEF";
~~~

## MP.5 - Descriptor Matching
Now that the keypoints are detected and are accompanied by their corresponding descriptors, we need to match keypoints between two different frames. We can use a FLANN matcher or a k-nearest neighbor matching method. If a FLANN matcher is selected in the main function, the following code will asign the pointer for a cv::DescriptorMatcher to a Flann based matcher. 
~~~
matcher = cv::FlannBasedMatcher::create();
~~~
If a KNN method is selected in the main code block, we will assign the matcher pointer to a KNN-matcher as can be seen below. The next section will then show how we impleent the descriptor distance ratio test in order to decide if we keep an associated pair of keypoints. 
~~~
int k = 2;
vector<vector<cv::DMatch>> knnMatches;
matcher->knnMatch(descSource, descRef, knnMatches, k);
~~~
## MP.6 - Descriptor Distance Ratio
Now that the nearest neighbors are found, we need to decide if they are near enough to be matches. We use the descriptor distance ratio, of Lowes ratio test, with a threshold value of 0.8 in order to do this. It checks the best match vs. the second best match in order to decide if the keypoints that are matched are sufficiently unique. 
~~~
// Lowes ratio test from FLANN matcher OpenCV Documentation
float ratioThresh = 0.8;

for (size_t i = 0; i < knnMatches.size(); i++)
{
    if (knnMatches[i][0].distance < ratioThresh * knnMatches[i][1].distance)
    {
        matches.push_back(knnMatches[i][0]);
    }
}
~~~
## MP.7 - Performance Evaluation 1
The number of total detected keypoints on the preceding vehicle were counted for each of the ten images and for each of the detectors that were implemented. I found that the best detector was FAST, followed by BRISK, and finally AKAZE. This can be seen below as well as the average number of detections on the preceding vehicle over the 10 images. 
1. FAST - 402 Keypoint Average
2. BRISK - 274 Keypoint Average
3. AKAZE - 168 Keypoint Average

## MP.8 - Performance Evaluation 2
The number of matched keypoints for all 10 images using all possible combinations of detectors and descriptors were then counted. For matching, a brute force method was used with a descriptor distance ratio of 0.8. The BRIEF descriptor consistently had the highest number of matches, followed by ORB and then BRISK across the different detectors. 

## MP.9 - Performance Evaluation 3
The time it took for each keypoint detection and descriptor extraction was also calculated for all possible combinations. The resuls are in the spread sheet 2D_feature_tracking. Based on this, I used a metric that calculated the number of successful matches per millisecond. This gives us a practical idea of what would be most ideal for a system in which time is crucial (like our collission detection system). 
With these results, the best combination of detectors and descriptors were:
1. FAST with ORB - 742.69 Matches / ms
2. FAST with BRIEF - 492.99 Matches / ms
3. FAST with BRISK - 298.19 Matches / sms















