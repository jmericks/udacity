/* \Jonny Erickson */
// PCL lib Functions for processing point clouds 

#ifndef PROCESSPOINTCLOUDS_H_
#define PROCESSPOINTCLOUDS_H_

#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/crop_box.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/transforms.h>
#include <iostream> 
#include <string>  
#include <vector>
#include <ctime>
#include <chrono>
#include "render/box.h"
#include <unordered_set>
//#include "kdtree.h"

// Structure to represent node of kd tree
struct Node
{
	std::vector<float> point;
	int id;
	Node* left;
	Node* right;

	Node(std::vector<float> arr, int setId)
	:	point(arr), id(setId), left(NULL), right(NULL)
	{}
};
template <typename PointT>
struct KdTreeCustom
{
	Node* root;

	KdTreeCustom()
	: root(NULL)
	{}
	
	//double pointer, looking at the memory address of the current node of the tree
	void insertHelper(Node *&node, uint depth, PointT point, int id)
	{
		if(node==NULL)
		{
			std::vector<float>v_point(point.data, point.data+3);
			node = new Node(v_point, id);
		}
		else
		{
			// modulus with tell us to look at x, y, or z values
			uint insertDepth = depth % 3;

			// brand new x value compared to the current x value of the node we are at

			if(point.data[insertDepth] < node->point[insertDepth])
			{
				insertHelper(node->left, depth+1, point, id);
			}
			else
			{
				insertHelper(node->right, depth+1, point, id);

			}	
		}
		
	}
	
	void insert(typename pcl::PointCloud<PointT>::Ptr cloud)
	{
		for(int idx = 0; idx < cloud->points.size(); idx++){
			insertHelper(root,0,cloud->points[idx],idx);
		}
	}

	void searchHelper(PointT target, Node *&node, uint depth, float distanceTol, std::vector<int> *ids)
	{
		if(node!=NULL)
		{

			
			if( 
			(node->point[0]>=(target.data[0]-distanceTol)&& node->point[0]<=(target.data[0]+distanceTol)) && 
			(node->point[1]>=(target.data[1]-distanceTol)&& node->point[1]<=(target.data[1]+distanceTol)) &&
			(node->point[2]>=(target.data[2]-distanceTol)&& node->point[2]<=(target.data[2]+distanceTol))
			)
			{
				float distance = sqrt(
				(node->point[0]-target.data[0])*(node->point[0]-target.data[0])+
				(node->point[1]-target.data[1])*(node->point[1]-target.data[1])+
				(node->point[2]-target.data[2])*(node->point[2]-target.data[2])
				);

				if (distance <= distanceTol)
					ids->push_back(node->id);
			}

			uint searchDepth = depth % 3;

			if (target.data[searchDepth]-distanceTol < node->point[searchDepth])
			{
				searchHelper(target, node->left, depth+1, distanceTol, ids);
			}
			if (target.data[searchDepth]+distanceTol > node->point[searchDepth])
			{
				searchHelper(target, node->right, depth+1, distanceTol, ids);
			}
			

		}

	}
	// return a list of point ids in the tree that are within distance of target
	std::vector<int> search(PointT target, float distanceTol)
	{
		std::vector<int> ids;
		searchHelper(target, root, 0, distanceTol, &ids);
		// want the indices of points that are near by
		return ids;
	}
	

};
template<typename PointT>
class ProcessPointClouds {
public:

    //constructor
    ProcessPointClouds();
    //deconstructor
    ~ProcessPointClouds();

    void numPoints(typename pcl::PointCloud<PointT>::Ptr cloud);

    typename pcl::PointCloud<PointT>::Ptr FilterCloud(typename pcl::PointCloud<PointT>::Ptr cloud, float filterRes, Eigen::Vector4f minPoint, Eigen::Vector4f maxPoint);

    std::pair<typename pcl::PointCloud<PointT>::Ptr, typename pcl::PointCloud<PointT>::Ptr> SeparateClouds(pcl::PointIndices::Ptr inliers, typename pcl::PointCloud<PointT>::Ptr cloud);

    std::pair<typename pcl::PointCloud<PointT>::Ptr, typename pcl::PointCloud<PointT>::Ptr> SegmentPlane(typename pcl::PointCloud<PointT>::Ptr cloud, int maxIterations, float distanceThreshold);

    std::pair<typename pcl::PointCloud<PointT>::Ptr, typename pcl::PointCloud<PointT>::Ptr> PlanarRansac(typename pcl::PointCloud<PointT>::Ptr cloud, int maxIterations, float distanceTol);

    void clusterHelper(typename pcl::PointCloud<PointT>::Ptr cloud, int indice, std::vector<int>& cluster, std::vector<bool>& processed, typename KdTreeCustom<PointT>::KdTreeCustom* tree, float distanceTol);

    std::vector<typename pcl::PointCloud<PointT>::Ptr> euclideanCluster(typename pcl::PointCloud<PointT>::Ptr cloud, float distanceTol, int minSize, int maxSize);

    std::vector<typename pcl::PointCloud<PointT>::Ptr> Clustering(typename pcl::PointCloud<PointT>::Ptr cloud, float clusterTolerance, int minSize, int maxSize);

    Box BoundingBox(typename pcl::PointCloud<PointT>::Ptr cluster);

    void savePcd(typename pcl::PointCloud<PointT>::Ptr cloud, std::string file);

    typename pcl::PointCloud<PointT>::Ptr loadPcd(std::string file);

    std::vector<boost::filesystem::path> streamPcd(std::string dataPath);
  
};
#endif /* PROCESSPOINTCLOUDS_H_ */